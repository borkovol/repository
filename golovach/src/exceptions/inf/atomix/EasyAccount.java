package exceptions.inf.atomix;


interface EasyAccount {
    public void change(int delta) throws TryAgainEx, ZedDeadBabyEx;
}
