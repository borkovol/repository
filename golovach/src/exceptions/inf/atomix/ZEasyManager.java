package exceptions.inf.atomix;

public class ZEasyManager implements EasyManager {
    @Override
    public boolean transfer(EasyAccount[] accounts, int[] money) {


        for (int j = 0; j < money.length; j++) {
            try {
                accounts[j].change(money[j]);
            } catch (TryAgainEx tryAgainEx) {
                j-=1;
            } catch (ZedDeadBabyEx zedDeadBabyEx) {
                if(j>0)
                {
                    int[] amountsToRevert = new int[j];
                    for (int k = 0; k < j; k++) {
                        amountsToRevert[k] = -money[k];
                    }
                    transfer(accounts, amountsToRevert);
                }
                return false;
            }
        }

        return true;

    }

}
