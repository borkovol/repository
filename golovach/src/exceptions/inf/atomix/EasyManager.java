package exceptions.inf.atomix;

public interface EasyManager {
    public boolean transfer(EasyAccount[] accounts, int[] money);
}
