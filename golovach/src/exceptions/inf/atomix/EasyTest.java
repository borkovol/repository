package exceptions.inf.atomix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class EasyTest {

    public static void main(String[] args) {
        for (int k = 0; k < 1000000; k++) {
            final Random rnd = new Random(k);
            EasyManager manager = new ZEasyManager();
            final AtomicBoolean zedDead = new AtomicBoolean(false);
            XEasyAccount[] accounts = {
                    new XEasyAccount(rnd, zedDead),
                    new XEasyAccount(rnd, zedDead),
                    new XEasyAccount(rnd, zedDead)};
            int[] money = {-1, -1, +2};
            boolean ok = manager.transfer(accounts, money);
            if (ok) {
                for (int i = 0; i < accounts.length; i++) {
                    if(accounts[i].getTotalChange() != money[i]){
                        throw new AssertionError("Fail commit:" + Arrays.toString(accounts));
                    }
                }
            } else {
                if(!zedDead.get()){
                    throw new AssertionError("Zed alive, but rollback!");
                }
                for (int i = 0; i < accounts.length; i++) {
                    if(accounts[i].getTotalChange() != 0){
                        throw new AssertionError("Fail rollback:" + Arrays.toString(accounts));
                    }
                }
            }
        }
        System.out.println("OK!");
    }
}

class XEasyAccount implements EasyAccount{

    private final Random rnd;
    private final AtomicBoolean zedDead;
    private int totalChange = 0;
    private List<Integer> history = new ArrayList<Integer>();
    private boolean imDead = false;

    XEasyAccount(Random rnd, AtomicBoolean zedDead) {
        this.rnd = rnd;
        this.zedDead = zedDead;
    }

    @Override
    public void change(int delta) throws TryAgainEx, ZedDeadBabyEx{
        if(imDead){
            throw new ZedDeadBabyEx("This account is totally dead!");
        }
        int r = rnd.nextInt(100);
        if(r == 0 && !zedDead.get()){
            zedDead.set(true);
            imDead = true;
            throw new ZedDeadBabyEx("First time!");
        } else if (r < 50){
            throw new TryAgainEx();
        } else {
            totalChange += delta;
            history.add(delta);
        }
    }

    int getTotalChange(){
        return totalChange;
    }

    @Override
    public String toString() {
        return "Acc[totalChange=" + totalChange + ", history=" + history + "]";
    }
}
