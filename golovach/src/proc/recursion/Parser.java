package proc.recursion;

public class Parser {

    public static void main(String[] args) {
//        System.out.println(Parser.eval("-1"));

//        System.out.println("  >> 123 = " + Parser.eval("123"));
//        System.out.println("  >> 2*3 = " + Parser.eval("2*3"));
//        System.out.println("  >> 2*(1+3) = " + Parser.eval("2*(1+3)"));
//        System.out.println("  >> 1+(5-2*(13/6)) = " + Parser.eval("1+(5-2*(13/6))"));

//        System.out.println("  >> 123 = " + Parser.eval("123"));
//        System.out.println("  >> 2*3 = " + Parser.eval("2*3"));
//        System.out.println("  >> (1+3)*2 = " + Parser.eval("(1+3)*2"));
//        System.out.println("  >> ((13/6)*2-5)+1 = " + Parser.eval("((13/6)*2-5)+1"));

    }

    private static int eval(String expr) {
        if(expr.startsWith("(") && expr.endsWith(")")){
            int value = eval(expr.substring(1, expr.length() - 1));
            System.out.println(expr + "=" + value);
            return value;
        } else if (expr.startsWith("(") && !expr.endsWith(")")){
//            throw new RuntimeException("If expr starts with '(' then MUST end with ')'");
            int pos = expr.length() - 1;
            while (pos > 0){
                if (Character.isDigit(expr.charAt(pos))){
                    pos--;
                } else {
                    int rightOperand = Integer.valueOf(expr.substring(pos + 1));
                    char operation = expr.charAt(pos);
                    int leftOperand = eval(expr.substring(0, pos));
                    switch (operation){
                        case '+':
                            System.out.println(expr + "=" + (leftOperand + rightOperand));
                            return leftOperand + rightOperand;
                        case '-':
                            System.out.println(expr + "=" + (leftOperand - rightOperand));
                            return leftOperand - rightOperand;
                        case '*':
                            System.out.println(expr + "=" + (leftOperand * rightOperand));
                            return leftOperand * rightOperand;
                        case '/':
                            System.out.println(expr + "=" + (leftOperand / rightOperand));
                            return leftOperand / rightOperand;
                    }
                }
            }
        } else {
            int pos = 0;
            while (pos < expr.length() - 1){
                if (Character.isDigit(expr.charAt(pos))){
                    pos++;
                } else {
                    int leftOperand = Integer.valueOf(expr.substring(0, pos));
                    char operation = expr.charAt(pos);
                    int rightOperand = eval(expr.substring(pos + 1));
                    switch (operation){
                        case '+':
                            System.out.println(expr + "=" + (leftOperand + rightOperand));
                            return leftOperand + rightOperand;
                        case '-':
                            System.out.println(expr + "=" + (leftOperand - rightOperand));
                            return leftOperand - rightOperand;
                        case '*':
                            System.out.println(expr + "=" + (leftOperand * rightOperand));
                            return leftOperand * rightOperand;
                        case '/':
                            System.out.println(expr + "=" + (leftOperand / rightOperand));
                            return leftOperand / rightOperand;
                    }
                }
            }
        }
        return Integer.valueOf(expr);
    }

}
