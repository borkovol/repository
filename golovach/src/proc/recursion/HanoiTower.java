package proc.recursion;


import java.util.Stack;

public class HanoiTower {

    public static void main(String[] args) {
        Stack<Integer> from = new Stack<Integer>();
        Stack<Integer> help = new Stack<Integer>();
        Stack<Integer> to = new Stack<Integer>();

        from.push(5);
        from.push(4);
        from.push(3);
        from.push(2);
        from.push(1);

        System.out.println(from);
        System.out.println(help);
        System.out.println(to);

        exchange(from, help, to, from.size());

        System.out.println(from);
        System.out.println(help);
        System.out.println(to);

    }
//    TODO: consider a condition that bigger numbers can't fall on smaller numbers
//    TODO: make a method which fill the "from" stack up to necessary value
    private static void exchange(Stack<Integer> from, Stack<Integer> help, Stack<Integer> to, int count) {
        if(count > 0){
            exchange(from, to, help, count - 1);
            int biggest = from.pop();
            to.push(biggest);
            exchange(help, from, to, count - 1);
        }
    }

}
